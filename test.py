# -*- coding: utf-8 -*-

import sys, urllib, httplib, time

# 配置
interface_url = 'www.soj.me'
interface_path = '/submit.php?problem_id=1000'
text='''// Problem#: 1000
// Submission#: 2284089
// The source code is licensed under Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
// URI: http://creativecommons.org/licenses/by-nc-sa/3.0/
// All Copyright reserved by Informatic Lab of Sun Yat-sen University
#include<iostream>
using namespace ::std;

int main() {
    int a = 0, b = 0;
    cin >> a >>b;
    cout << a - b << endl;
    return 0;
}                                 '''
def make_post():
    conn = httplib.HTTPConnection(interface_url)
    post_data = text
    headers = { "Content-type": "text/html","Content-Length": "%d" % len(post_data)}
    conn.request("POST", interface_path,"", headers)
    print post_data
    conn.send(post_data)
    response = conn.getresponse()
    #print response.status, response.reason
    print response.read()
    conn.close()

def main(argv):
    make_post()

main(sys.argv)
