#-*-coding:utf-8-*-
import re  #python中使用正则表达式需导入此库
import urllib2

url = "http://www.happytreefriendscn.com/download/"

href = 'href="(.*?)"'  #定义正则表达式的句法规则
href_re = re.compile(href)  #通过compile函数“编译”正则表达式

res = urllib2.urlopen(url)
data = res.read()
href_info = href_re.findall(data)

f = open('result.txt', 'w+')

for item in href_info:
    if '.flv' in item:
        print item
        f.write(item + '\n')
